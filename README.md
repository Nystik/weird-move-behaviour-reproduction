# Test

1. Run build.bat
2. DLL files are built and moved to `folder1` and `folder2`

**Expected behaviour**

`lib_32.dll` and `lib.dll` should be different

**Observed behaviour**

`lib_32.dll` and `lib.dll` are identical

---

If the binaries are built seperately using `build_32.bat` and `build_64.bat` the issue does not occur.
