: close echo
@echo off

: format code before compile
cargo fmt

: remove old files if folder exists
if exist .\folder1 (
     rmdir /Q /S .\folder1
     mkdir .\folder1
) else (
    mkdir .\folder1
)

if exist .\target (
     rmdir /Q /S .\target
)

: compile 32-bit dll
cargo +stable-i686-pc-windows-msvc build --release
move .\target\release\lib.dll .\folder1\lib_32.dll