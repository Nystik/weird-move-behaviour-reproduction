: close echo
@echo off

: format code before compile
cargo fmt

: remove old files if folder exist
if exist .\folder2 (
     rmdir /Q /S .\folder2
     mkdir .\folder2
) else (
    mkdir .\folder2
)

if exist .\target (
     rmdir /Q /S .\target
)

: compile 64-bit dll
cargo +stable-x86_64-pc-windows-msvc build --release
move .\target\release\lib.dll .\folder2\lib.dll